var gulp = require('gulp'),
  sass = require('gulp-sass')(require('sass')),
  rename = require('gulp-rename'),
  prefix = require('gulp-autoprefixer'),
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify'),
  sourcemaps = require('gulp-sourcemaps'),
  sassLint = require('gulp-sass-lint'),
  csscombx = require('gulp-csscombx'),
  cssmin = require('gulp-cssmin');

var onError = function (err) {
  notify.onError({
    title: "Gulp",
    subtitle: "Failure!",
    message: "Error: <%= error.message %>",
    sound: "Basso"
  })(err);
  this.emit('end');
};


// BUILD SUBTASKS
// ---------------

gulp.task('styles', function () {
  return gulp.src('scss/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    // .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(prefix())
    // .pipe(rename('basic-paragraphs.css'))
    // .pipe(sourcemaps.write('.'))
    .pipe(csscombx())
    .pipe(gulp.dest('css'))
});

// styles-min:
gulp.task('styles-min', function () {
  return gulp.src('scss/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sass())
    .pipe(prefix())
    .pipe(cssmin())
    .pipe(rename({
      extname: ".min.css"
    }))
    .pipe(gulp.dest('css'))
});

// sass-lint:
gulp.task('sass-lint', function () {
  return gulp.src('scss/**/*.scss')
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});

// BUILD TASKS
// ------------

// build:
gulp.task(
  'build',
  gulp.series(
    'styles'
  )
);

// watch:
gulp.task(
  'watch',
  function () {
    gulp.watch(
      'scss/**/*.scss',
      gulp.series(
        // 'sass-lint',
        'styles'
      )
    );
  });