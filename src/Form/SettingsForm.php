<?php

namespace Drupal\basic_paragraphs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Basic paragraph - Styles settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'basic_paragraphs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['basic_paragraphs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['column'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Columns styles'),
      '#default_value' => $this->config('basic_paragraphs.settings')->get('column'),
    ];
    $form['bg_img'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Background image styles'),
      '#default_value' => $this->config('basic_paragraphs.settings')->get('bg_img'),
    ];
    // $form['example'] = [
    //   '#type' => 'fieldset',
    //   '#title' => $this->t('some examples'),
    //   '#markup' => '
    //   <strong>1 column:</strong>
    //   <div class="paragraph--type--bp-1-column">
    //     <div class="field">1 column example, row 1</div>
    //   </div>
    //   <strong>2 column:</strong>
    //   <div class="paragraph--type--bp-2-column">
    //     <div class="field">2 column example, row 1</div>
    //     <div class="field">2 column example, row 2</div>
    //   </div>
    //   <strong>3 column:</strong>
    //   <div class="paragraph--type--bp-3-column">
    //     <div class="field">3 column example, row 1</div>
    //     <div class="field">3 column example, row 2</div>
    //     <div class="field">3 column example, row 3</div>
    //   </div>
    //   ',
    // ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (($form_state->getValue('column') != 0) && ($form_state->getValue('column') != 1)) {
      $form_state->setErrorByName('column', $this->t('The value is not correct.'));
    }
    if (($form_state->getValue('bg_img') != 0) && ($form_state->getValue('bg_img') != 1)) {
      $form_state->setErrorByName('bg_img', $this->t('The value is not correct.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('basic_paragraphs.settings')
      ->set('column', $form_state->getValue('column'))
      ->save();
    $this->config('basic_paragraphs.settings')
      ->set('bg_img', $form_state->getValue('bg_img'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
